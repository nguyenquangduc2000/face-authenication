import numpy as np
import cv2

cap = cv2.VideoCapture('http://192.168.42.129:8080/video/mjpeg')

while(cap.isOpened()):
    ret, image = cap.read()
    # loadedImage = cv2.imdecode(image, cv2.IMREAD_COLOR)
    cv2.imshow('frame',image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
