import classify
import sys
import cv2
import preprocess
import time
import os
from _thread import *
import time
import openpyxl

classifier = classify.Classify()
preprocessor = preprocess.PreProcessor()
camera = cv2.VideoCapture(0)
# camera = cv2.VideoCapture("http://192.168.42.129:8080/video/mjpeg")
start = time.time()

font = cv2.FONT_HERSHEY_SIMPLEX
num_each_face = 30
excel_file = "../DS-Tham-gia-cuoc-thi-dua-xe-IOT-mo-hinh.xlsx"
threshold = 0.2

def capture_train():
    global classifier
    global preprocess
    global camera

    name = input("Input name: ")
    if not os.path.exists('./data/'+name):
        os.makedirs('./data/'+name)

    print('Capturing faces...')
    i = 0
    while i < num_each_face:
        print('Capturing step: %s' % i)
        return_value, image = camera.read()
        bb, pp_image, run_detect = preprocessor.align(image, training=True)

        if (run_detect):
            cv2.imwrite('./data/' + name + '/' + str(i) + '.jpg', pp_image[0])
            i += 1

        time.sleep(0.3)

    print('Capture done!')

    # print('Training new face...')
    # os.system('python classifier.py')
    # print('Training complete!')

    # classifier = classify.Classify()

def process_click(event, x, y,flags, params):
    # check if the click is within the dimensions of the button
    if event == cv2.EVENT_LBUTTONDOWN:
        if y > params[1] and y < params[3] and x > params[0] and x < params[2]:
            start_new_thread(capture_train, ())

def main():
    global classifier
    global camera
    global preprocess
    global threshold

    cv2.namedWindow('FaceRecognition')
    wb = openpyxl.load_workbook(excel_file)
    ds = wb.active
    ds_mssv = [str(row[0].value) for row in ds.rows]

    def checkin(mssv):
        for row in ds.rows:
            if str(row[0].value) == mssv and not row[6].value:
                to_checkin = input("Checked in %s (y/n): " % mssv)
                if (to_checkin == 'y'):
                    row[6].value = 'x'
                    wb.save(excel_file)
                break

    while camera.isOpened():
        return_value, image = camera.read()
        bb, pp_image, run_detect = preprocessor.align(image)

        if (run_detect):
            for i in range(len(bb)):
                cv2.rectangle(image, (bb[i][0],bb[i][1]), (bb[i][2],bb[i][3]), (0, 255, 0), 2)

                name = classifier.predict(pp_image[i], threshold)
                if name in ds_mssv:
                    start_new_thread(checkin, tuple([name]))

                cv2.putText(image, name, (bb[i][0] + 5,bb[i][3] - 5), font, 0.8, (0, 255, 0), 1, cv2.LINE_AA)
                params = bb[i]

                if name == "Unknown face":
                    cv2.setMouseCallback('FaceRecognition', process_click, params)

        cv2.imshow('FaceRecognition', image)

        waitKey = cv2.waitKey(1)

        if waitKey & 0xFF == ord('t'):
            print('Training new face...')
            os.system('python classifier.py')
            print('Training complete!')

            classifier = classify.Classify()
        elif waitKey & 0xFF == ord('+'):
            threshold += 0.02
            if threshold > 1: threshold = 1
            print("Current threshold: %f" % threshold)
        elif waitKey & 0xFF == ord('-'):
            threshold -= 0.02
            if threshold < 0: threshold = 0
            print("Current threshold: %f" % threshold)
        elif waitKey & 0xFF == ord('q'):
            break

    end_time = time.time()
    print("Run time: %f" % (end_time-start))

    # When everything done, release the capture
    camera.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
